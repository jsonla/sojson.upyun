package com.sojson.upyun;

import main.java.com.UpYun;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class Token {

    @Value("${upyun.password}")
    String password;
    @Value("${upyun.userName}")
    String userName;
    @Value("${upyun.bucketName}")
    String bucketName;
    @Value("${upyun.domain}")
    public static  String domain;



    public UpYun get(){
        UpYun upyun = new UpYun(bucketName, userName, password);
        //是否开启 debug 模式：默认不开启
//		upyun.setDebug(true);
        //手动设置超时时间：默认为30秒
        upyun.setTimeout(30);

        /**
         UpYun.ED_AUTO    //根据网络条件自动选择接入点
         UpYun.ED_TELECOM //电信接入点
         UpYun.ED_CNC     //联通网通接入点
         UpYun.ED_CTT     //移动铁通接入点
         */
        //选择最优的接入点
        upyun.setApiDomain(UpYun.ED_AUTO);
        return upyun;
    }



}
