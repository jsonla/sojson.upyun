package com.sojson.upyun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class UpyunApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpyunApplication.class, args);
	}
}
