package com.sojson.upyun.execute;


import com.sojson.upyun.Token;
import lombok.extern.slf4j.Slf4j;
import main.java.com.UpYun;
import main.java.com.upyun.UpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * 删除
 */
@Slf4j
@Component
public class Delete {

    @Autowired
    Token token;

    /**
     * 根据目录删除，如果目录内容过多，请悠着点使用
     * @param dir   目录 如  ： images/
     * @return
     */
    public Boolean deleteFileBydir(String dir){
        dir = dir.endsWith("/")?dir:dir + "/";
        try {


           return  token.get().rmDir(dir);
//            List<UpYun.FolderItem> list = token.get().readDir(dir);
//            if(null != list){
//                String finalDir = dir;
//                list.forEach(e -> {
//                    deleteFile(String.format("%s%s", finalDir,e.name));
//                });
//            }
        } catch (IOException | UpException e) {
            log.error("删除文件出错。",e);
            return Boolean.FALSE;
        }
    }

    /**
     * 根据文件路径删除，
     * @param filePath  文件全路径，如  images/publish/1.gif
     * @return
     */
    public Boolean deleteFile(String  filePath){
        try {
            return token.get().deleteFile(filePath);
        } catch (IOException | UpException e) {
            log.error("删除文件出错。",e);
            return Boolean.FALSE;
        }
    }
}
