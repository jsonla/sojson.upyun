package com.sojson.upyun.execute;

import com.sojson.upyun.Token;
import lombok.extern.slf4j.Slf4j;
import main.java.com.upyun.UpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Slf4j
@Component
public class DownLoad {

    @Autowired
    Token token;

    /**
     * 读取远程文件内容，适用于 文本文件
     * @param remoteFilePath    远程位置
     * @return
     */
    public String readFile(String remoteFilePath){
        try {
            return token.get().readFile(remoteFilePath);
        } catch (Exception e) {
            log.error("读取文件出错,path:{}",remoteFilePath);
            return null;
        }
    }

    /**
     * 下载文件到本地
     * @param remoteFilePath    远程位置
     * @param localFilePath     本地位置
     * @return
     */

    public Boolean downLoadFile(String remoteFilePath ,String localFilePath){
        File file = new File(localFilePath); // 创建一个本地临时文件
        try {
            return  token.get().readFile(remoteFilePath, file);
        } catch (IOException | UpException e) {
            log.error("下载文件出错,remoteFilePath:{},localFilePath:{}",remoteFilePath,localFilePath);
            return Boolean.FALSE;
        }
    }
}
