package com.sojson.upyun.execute;

import com.sojson.upyun.Token;
import lombok.extern.slf4j.Slf4j;
import main.java.com.upyun.UpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Slf4j
@Component
public class Get {

    /**
     * 获取文件信息
     *
     */
    @Autowired
    Token token;
    public Map<String, String> fileInfo(String filePath){

        try {
            return  token.get().getFileInfo(filePath);
        } catch (IOException |UpException e) {
           log.error("获取文件信息出现错误。",e);
        }

        return null;
    }
}
