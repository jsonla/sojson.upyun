package com.sojson.upyun.execute;

import com.sojson.upyun.Token;
import com.sojson.upyun.utils.Tools;
import lombok.extern.slf4j.Slf4j;
import main.java.com.upyun.UpException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@Slf4j
@Component
public class Up {

    @Autowired
    Token token;


    /**
     * 文本上传
     * @param path  路径 + 文件名
     * @param txt   文本内容
     * @return
     */
    public Boolean upByString(String path,String txt){
        if(StringUtils.isEmpty(path) || StringUtils.isEmpty(txt)){
            return Boolean.FALSE;
        }
        try {
            return  token.get().writeFile(path,txt, true);
        } catch (IOException | UpException e) {
            log.error("上传文本出现异常,path{},txt:{}",e,path,txt);
            return Boolean.FALSE;
        }
    }

    /**
     * 上传文件
     * @param path  文件路径 + 文件名
     * @param file  文件
     * @return
     */
    public Boolean upByFile(String path,File file){
        if(StringUtils.isEmpty(path) || null == file){
            return Boolean.FALSE;
        }
        try {
            return  token.get().writeFile(path,file, true);
        } catch (IOException | UpException e) {
            log.error("上传文本出现异常,path{},fileName:{}",e,path,file.getName());
            return Boolean.FALSE;
        }
    }

    /**
     * 上传文件，Byte[]
     * @param  path 文件路径 + 文件名
     * @param datas file Byte[]
     * @return
     */
    public Boolean upByByte(String path,byte[] datas){
        if(StringUtils.isEmpty(path) || null == datas){
            return Boolean.FALSE;
        }
        try {
            return  token.get().writeFile(path,datas, true);
        } catch (IOException | UpException e) {
            log.error("上传 byte[]出现异常,path{},",e,path);
            return Boolean.FALSE;
        }
    }
    /**
     * 上传网络文件[重命名] 文件名指定
     *
     * @param path [保存的路径]
     *
     * @param keyName [文件名，可以为 null]
     * @return 文件网络路径
     */
    public  Boolean upByWeb(String path, String urlPath, String keyName){
        Boolean result = Boolean.FALSE;
        if (StringUtils.isBlank(keyName)) {
            keyName = Tools.getFileName(urlPath);
        }


        URL url;
        HttpURLConnection httpConn = null;
        InputStream input = null ;
        try {
            url = new URL(urlPath);
            httpConn = (HttpURLConnection) url.openConnection();
            httpConn.addRequestProperty("Referer","http://image.baidu.com/search/index?ct=201326592&cl=2&st=-1&lm=-1&nc=1&ie=utf-8&tn=baiduimage&ipn=r&rps=1&pv=&fm=rs1&word=%E7%BE%8E%E5%A5%B3%E5%9B%BE%E7%89%87&oriquery=%E5%9B%BE%E7%89%87&ofr=%E5%9B%BE%E7%89%87&sensitive=0");
            httpConn.addRequestProperty("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko/20100101 Firefox/57.0");
            httpConn.connect();
            input = httpConn.getInputStream();

            return  token.get().writeFile(path + keyName, input2byte(input), true);
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }finally {
            try {
                input.close();
                httpConn.disconnect();
                url = null;
                httpConn = null;
            }catch (Exception e){}

        }





    }

    public static final byte[] input2byte(InputStream inStream)
            throws IOException {
        ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
        byte[] buff = new byte[100];
        int rc;
        while ((rc = inStream.read(buff, 0, 100)) > 0) {
            swapStream.write(buff, 0, rc);
        }
        byte[] in2b = swapStream.toByteArray();
        return in2b;
    }


}
