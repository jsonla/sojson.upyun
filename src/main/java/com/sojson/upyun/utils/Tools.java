package com.sojson.upyun.utils;


import com.sojson.upyun.Token;
import org.apache.commons.lang.StringUtils;

/**
 * 公共静态方法
 * @author zhou-baicheng
 *
 */
public class Tools {
	
	/**
	 * 返回上传后生成的url
	 * @param key   文件名
	 * @return 全URL
	 */
	public static String getSourcePath(String key){
		return "https://"+ Token.domain +  key;
	}
	/**
	 * 根据文件路径获取文件名
	 * @param filePath 文件路径
	 * @return 文件名
	 */
	public static String getFileName(String filePath){
		return filePath.substring(filePath.lastIndexOf("/")+1);
	}
	/**
	 * 获取文件后缀   如.jpg
	 * @param originalFilename
	 * @return
	 */
	public static String getFileSuffix(String originalFilename) {
		if(StringUtils.isBlank(originalFilename))
			return null;
		return originalFilename.substring(originalFilename.lastIndexOf(".")).toLowerCase();
	}
}
