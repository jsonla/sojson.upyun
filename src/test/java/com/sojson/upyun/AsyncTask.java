package com.sojson.upyun;


import com.sojson.upyun.execute.Up;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


@Component
public class AsyncTask {
    @Autowired
    Up up;
    @Async
    public void doTask1(int i) throws InterruptedException{

        boolean result = up.upByString("/code32/21333333333333333332133Te"+i,String.valueOf(i));
        System.out.println(result);
        System.out.println(("Task"+i+" started."));
    }
}
