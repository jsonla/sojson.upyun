package com.sojson.upyun;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "spring.task.pool")
public class TaskThreadPoolConfig {

    private int corePoolSize = 10;

    private int maxPoolSize = 30;

    private int keepAliveSeconds = 60;

    private int queueCapacity = 10;
}
