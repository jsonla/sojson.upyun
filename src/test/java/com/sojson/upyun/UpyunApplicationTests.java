package com.sojson.upyun;

import com.sojson.upyun.execute.Delete;
import com.sojson.upyun.execute.DownLoad;
import com.sojson.upyun.execute.Up;
import main.java.com.UpYun;
import main.java.com.upyun.UpException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UpyunApplication.class)
public class UpyunApplicationTests {

	@Value("${upyun.password}")
	String password;
	@Value("${upyun.userName}")
	String userName;
	@Value("${upyun.bucketName}")
	String bucketName;



	@Autowired
	private AsyncTask asyncTask;

	@Autowired
	Up up;
	@Autowired
	DownLoad downLoad;
	@Autowired
	Delete delete;


	/**
	 * 多线程 上传
	 * @throws IOException
	 * @throws UpException
	 * @throws InterruptedException
	 */
	@Test
	public void thread() throws IOException, UpException, InterruptedException {
		long begin = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			asyncTask.doTask1(i);
		}
		System.out.println("主线程结束");
		long end = System.currentTimeMillis();

		System.out.println(end - begin);

		//主线程结束
		Thread.sleep(1000 * 60 * 5);
	}

	/**
	 * 测试网络上传
	 */
	@Test
	public void upByWebTest(){
		//百度图片上传
		Boolean result = up.upByWeb("images/", "http://img4.imgtn.bdimg.com/it/u=187826621,4174633557&fm=11&gp=0.jpg", "a.jpg");
		System.out.println(result);
	}

	/**
	 * 文本文件
	 */
	@Test
	public void upByTxtTest(){
		Boolean result = up.upByString("text/a.txt","我是一个文本文件");
		System.out.println(result);
	}

	/**
	 * 读文本文件内容
	 */
	@Test
	public void readFile(){
		String result = downLoad.readFile("text/a.txt");
		System.out.printf("text/a.txt内容为：%s\n",result);
	}

	/**
	 * 下载文件到本地
	 */
	@Test
	public void downLoadFile(){
		Boolean result = downLoad.downLoadFile("text/a.txt", "/Users/zhoubocheng/a.txt");
		System.out.println(result);
	}

	/**
	 * 删除文件
	 */
	@Test
	public void deleteByFile(){
		Boolean result = delete.deleteFile("text/a.txt");
		System.out.println(result);
	}

	/**
	 * 上传目录
	 */
	@Test
	public void deleteByDir(){
		Boolean result = delete.deleteFileBydir("text/");
		System.out.println(result);
	}


}
